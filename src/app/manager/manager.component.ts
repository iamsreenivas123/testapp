import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'app-manager',
    templateUrl: './manager.component.html',
    styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

    modalRef: BsModalRef;

    constructor(
        private router: Router, private modalService: BsModalService,
    ) { }

    ngOnInit() {
    }

    onShowManagerDetailsClick(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

}
